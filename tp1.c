#include <math.h>
#include <stdlib>

void matmul(int *mat1, int *mat2, double *matp, int tam)

int main void(){
    int tam=0;

    printf("Ingrese tamaño de matriz: ");
    scanf("%d", &tam);

    double matp[tam][tam], mat1[tam][tam], mat2[tam][tam];

    // Generación de matrices random
    srand(time(NULL));
    for (int i=0; i<tam; i++){
        for(int j=0; j<tam; j++){
            mat1[i][j] = rand()%101;
            mat2[i][j] = rand()%101;
        }
    }

    //presentacion de matrices

    printf("Las matrices son:");

    for(i=0;i<tam;i++){
	    for(j=0;j<tam;j++){
	        printf(" %.lf ",mat1[i][j]);
	    }
   		printf("\n");
	}


    for(i=0;i<tam;i++){
	    for(j=0;j<tam;j++){
	        printf(" %.lf ",mat2[i][j]);
	    }
   		printf("\n");
	}


            double mat_t[n][n];

            //Hacer matriz transversal

            for(i=0; i<n; i++){
                for(j=0; j<n; j++){
                    mat_t[j][i] = mat2[i][j];
                }
            }
}



    //Multiplicacion de matrices
    void matmul(double *mat1, double *mat2, double *matp, int n){

        for(i=0; i<n; i++){
            for(j=0; j<n; j++){
                matp[i*n+j] = 0;
                for(k=0; k<n; k++){
                    matp[i*n+j] += mat1[i*n+k] * mat2[k*n+i];
                }
            }
        }

    }

    void matmul_5(double *mat1, double *mat2, double *matp, int n){
        for(i=0; i<n; i++){
            for(j=0; j<n; j++){
                matp[i*n+j] = 0;
                for(k=0; k<n; k+=5){
                    matp[i*n+j] += mat1[i*n+k] * mat2[k*n+i] + mat1[i*n+k+1] * mat2[(k+1)*n+i] + mat1[i*n+k+2] * mat2[(k+2)*n+i] + mat1[i*n+k+3] * mat2[(k+3)*n+i] + mat1[i*n+k+4] * mat2[(k+4)*n+i];
                }
            }
        }
    }

    void matmul_10(double *mat1, double *mat2, double *matp, int n){
        for(i=0; i<n; i++){
            for(j=0; j<n; j++){
                matp[i*n+j] = 0;
                for(k=0; k<n; k+=10){
                    matp[i*n+j] += mat1[i*n+k] * mat2[k*n+i] + mat1[i*n+k+1] * mat2[(k+1)*n+i] + mat1[i*n+k+2] * mat2[(k+2)*n+i] + mat1[i*n+k+3] * mat2[(k+3)*n+i] + mat1[i*n+k+4] * mat2[(k+4)*n+i] + mat1[i*n+k+5] * mat2[(k+5)*n+i] + mat1[i*n+k+6] * mat2[(k+6)*n+i] + mat1[i*n+k+7] * mat2[(k+7)*n+i] + mat1[i*n+k+8] * mat2[(k+8)*n+i] + mat1[i*n+k+9] * mat2[(k+9)*n+i];
                }
            }
        }
    }


    //Funciones con matriz tranversal

    void matmul_t(double *mat1, double *mat2, double *matp, int n){

        double matt[n][n];

        //Hacer matriz transversal

        for(i=0; i<n; i++){
            for(j=0; j<n; j++){
                matt[j][i] = mat2[i][j];
            }
        }

        for(i=0; i<n; i++){
            for(j=0; j<n; j++){
                matp[i*n+j] = 0;
                for(k=0; k<n; k++){
                    matp[i*n+j] += mat1[i*n+k] * matt[i][k];
                }
            }
        }

    }

    void matmul_t_5(double *mat1, double *mat2, double *matp, int n){
        double matt[n][n];

        //Hacer matriz transversal

        for(i=0; i<n; i++){
            for(j=0; j<n; j++){
                matt[j][i] = mat2[i*n+j];
            }
        }

        //Multiplicacion
        for(i=0; i<n; i++){
            for(j=0; j<n; j++){
                matp[i*n+j] = 0;
                for(k=0; k<n; k+=5){
                    matp[i*n+j] += mat1[i*n+k] * matt[i][k] + mat1[i*n+k+1] * matt[i][k+1] + mat1[i*n+k+2] * matt[i][k+2] + mat1[i*n+k+3] * matt[i][k+3] + mat1[i*n+k+4] * matt[i][k+4];
                }
            }
        }
    }

    void matmul_t_10(double *mat1, double *matt, double *matp, int n){
        double matt[n][n];

        //Hacer matriz transversal

        for(i=0; i<n; i++){
            for(j=0; j<n; j++){
                mat_t[j][i] = matt[i][j];
            }
        }

        //Multiplicacion
        for(i=0; i<n; i++){
            for(j=0; j<n; j++){
                matp[i*n+j] = 0;
                for(k=0; k<n; k+=10){
                    matp[i*n+j] += mat1[i*n+k] * matt[i][k] + mat1[i*n+k+1] * matt[i][k+1] + mat1[i*n+k+2] * matt[i][k+2] + mat1[i*n+k+3] * matt[i][k+3] + mat1[i*n+k+4] * matt[i][k+4] + mat1[i*n+k+5] * matt[i][k+5] + mat1[i*n+k+6] * matt[i][k+6] + mat1[i*n+k+7] * matt[i][k+7] + mat1[i*n+k+8] * matt[i][k+8] + mat1[i*n+k+9] * matt[i][k+9];
                }
            }
        }
    }


/*NO10AS
    la memoria es lineal por lo tanto al indexar los lugares en memoria
    se realiza multiplicando 10l numero de fila (i)
    por la cantidad de columnas total (n)
    y sumando el numero de columna luego
*/

 /* Si tenemos un main con la siguiente estructura:
    int main(){
        double a[1000][1000], b[1000][1000], c[1000][1000].
    }

    probar crear matrices afuera y adentro de main
i*n+kimizacion -O 3 (suficiente)
    gnoplot
 t_10/
